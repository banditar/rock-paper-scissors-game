Colab, where the training script is:\
https://colab.research.google.com/drive/1dY1K8BSQRwJN7iIyVR3REtEdcISqUE_8?usp=sharing

# Rock-Paper-Scissors Game
:8ball:

József-Hunor Jánosi

Bachelor's Thesis project, Babes-Bolyai University, Cluj-Napoca, Romania (2021)

---

The user is able to play the game with a bot. User shows his hand to the camera, which detects the gesture, then makes a move as well.

The application's UI looks like the following:

![RPS app](imgs/rps_app.png)

---

CNN trained on self-made images of hand showing rock, paper, or scissors.
This model is used to predict images acquired from camera, while being wrapped in a game environment.
Keras' Sequential model was used. Also was compared to traditional machine learning models.

Sample images on which the model was trained:

![Sample RPS images](imgs/trained_images.png)
