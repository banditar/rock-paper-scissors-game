import numpy as np
import cv2
import os
import sys

PATH = os.getcwd()
cap = cv2.VideoCapture(0)

if len(sys.argv) < 4:
    print(f'Usage: {sys.argv[0]} label minlimit maxlimit')
else:
    label = sys.argv[1]

    SAVE_PATH = os.path.join(PATH, label)

    try:
        os.mkdir(SAVE_PATH)
    except FileExistsError:
        pass

    ct = int(sys.argv[2])
    maxCt = int(sys.argv[3])+1
    print("Hit Space to Capture Image")

    while True:
        ret, frame = cap.read()
        frame = cv2.flip(frame, 1)
        cv2.imshow('Get Data : '+label, frame[50:350, 100:400])
        if cv2.waitKey(1) & 0xFF == ord(' '):
            # path = SAVE_PATH+'/'+label+'{}.jpg'.format(ct)
            path = f'{SAVE_PATH}/{label}{ct}.jpg'
            cv2.imwrite(path, frame[50:350, 100:400])
            print(f'{path} Captured')
            ct += 1
        if ct >= maxCt:
            break

cap.release()
cv2.destroyAllWindows()
