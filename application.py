import tkinter
import cv2
import PIL.Image
import PIL.ImageTk
from keras.models import model_from_json
import numpy as np
from time import sleep
from random import randint


class App:
    def __init__(self):
        self.window = tkinter.Tk()
        self.window.title('Rock Paper Scissors')

        # conv net model
        self.load_model()

        self.vid = MyVideoCapture()

        self.player_score = 0
        self.bot_score = 0

        # canvases
        # game info canvas
        self.canvas_game = tkinter.Canvas(self.window)
        self.canvas_game.grid(row=0, column=0)

        # score and agent canvas
        self.canvas_score_agent = tkinter.Canvas(self.window)
        self.canvas_score_agent.grid(row=1, column=1)

        # cam canvas
        self.canvas_cam = tkinter.Canvas(
            self.window, width=self.vid.width, height=self.vid.height)
        self.canvas_cam.grid(row=1, column=0)

        # play button
        self.button_play = tkinter.Button(
            self.canvas_game, text='Play', command=self.play)
        self.button_play.grid(row=0, column=1)

        # agent
        self.label_agent = tkinter.Label(
            self.canvas_score_agent, text='Agent:')
        self.label_agent.grid(row=0, column=0)
        self.bot_mode = 1  # random

        def sel():
            self.bot_mode = radio_var.get()

        radio_var = tkinter.IntVar()
        self.radio_random = tkinter.Radiobutton(self.canvas_score_agent, text="Random", variable=radio_var, value=1,
                                                command=sel)
        self.radio_random.select()
        self.radio_random.grid(row=1, column=0)
        self.radio_cheater = tkinter.Radiobutton(self.canvas_score_agent, text="Cheater", variable=radio_var, value=2,
                                                 command=sel)
        self.radio_cheater.grid(row=2, column=0)

        # prediction
        self.label_predict = tkinter.Label(
            self.canvas_game, width=24)
        self.label_predict.grid(column=0, row=1)

        # counter
        self.label_counter = tkinter.Label(
            self.canvas_game, width=20)
        self.label_counter.grid(column=1, row=1)

        # bot played
        self.label_bot_played = tkinter.Label(
            self.canvas_game, width=18)
        self.label_bot_played.grid(column=2, row=1)

        # scores
        self.label_score = tkinter.Label(
            self.canvas_score_agent, text='Scores:')
        self.label_score.grid(row=4, column=1)

        self.label_player_score = tkinter.Label(
            self.canvas_score_agent, text=f'Player: {self.player_score}')
        self.label_player_score.grid(row=5, column=0)

        self.label_bot_score = tkinter.Label(
            self.canvas_score_agent, text=f'Bot: {self.bot_score}')
        self.label_bot_score.grid(row=5, column=2)

        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update()

        self.window.mainloop()

    def load_model(self):
        with open('model.json', 'r') as f:
            model_json = f.read()
        self.model = model_from_json(model_json)
        self.model.load_weights("model.h5")
        print("Loaded model from disk")

        shape_to_label = {
            'rock': np.array([1., 0., 0.]),
            'paper': np.array([0., 1., 0.]),
            'scissor': np.array([0., 0., 1.])
        }
        self.arr_to_shape = {
            np.argmax(shape_to_label[x]): x for x in shape_to_label.keys()}

    def play(self, counter=3):
        self.label_counter.config(text=f'Show a gesture in: {counter}')
        if counter > 0:
            self.window.after(1000, self.play, counter - 1)
            return

        self.label_counter.config(text='')

        roi = self.vid.get_gray_roi()

        prediction_arr = self.model.predict(roi)
        prediction_arr = [int(x*1000)/10 for x in prediction_arr[0]]
        prediction = self.arr_to_shape[np.argmax(prediction_arr)]

        self.label_predict.config(text=f'You are showing: {prediction}')

        bot_predict = self.bot_move(prediction)

        self.label_bot_played['text'] = f'Bot played: {bot_predict}'

        self.update_scores(prediction, bot_predict)

        print(f'Prediction = {prediction_arr}')

    def bot_move(self, player_move):
        if self.bot_mode == 1:
            # random
            move = self.arr_to_shape[randint(0, 2)]

        elif self.bot_mode == 2:
            # cheater
            move = self.get_opposite_move(player_move)

        return move

    def update_scores(self, player_move, bot_move):
        (score_a, score_b) = self.who_won(player_move, bot_move)
        self.player_score += score_a
        self.bot_score += score_b

        self.label_player_score['text'] = f'Player: {self.player_score}'
        self.label_bot_score['text'] = f'Bot: {self.bot_score}'

    def get_opposite_move(self, move):
        if move == 'rock':
            return 'paper'

        if move == 'paper':
            return 'scissor'

        if move == 'scissor':
            return 'rock'

    def who_won(self, move_a, move_b):
        if move_a == move_b:
            return (0, 0)

        if move_a == 'rock':
            if move_b == 'paper':
                # rock - paper
                return (0, 1)
            # rock - scissor
            return (1, 0)

        if move_a == 'paper':
            if move_b == 'scissor':
                # paper - scissor
                return (0, 1)
            # paper - rock
            return (1, 0)

        if move_b == 'rock':
            # scissor - rock
            return (0, 1)
        # scissor - papaer
        return (1, 0)

    def update(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            # roi
            cv2.rectangle(frame, (100, 50), (400, 350), (255, 255, 255), 2)
            self.photo = PIL.ImageTk.PhotoImage(
                image=PIL.Image.fromarray(frame))
            self.canvas_cam.create_image(
                0, 0, image=self.photo, anchor=tkinter.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self):
        # Open the video source
        self.vid = cv2.VideoCapture(0)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source")

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

    def get_gray_roi(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # Return a boolean success flag and the current frame converted to Gray
                return cv2.cvtColor(frame[50:350, 100:400], cv2.COLOR_BGR2GRAY).reshape(1, 300, 300, 1)
            else:
                return None
        else:
            return None

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


App()
