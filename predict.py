from keras.models import model_from_json
import numpy as np
from skimage import io
import cv2


def prepImg(pth):
    dim = 150
    color = False
    norm = True

    if color == False:
        channel = 1
    else:
        channel = 3

    pth = cv2.resize(pth, (dim, dim))

    if color == False:
        pth = cv2.cvtColor(pth, cv2.COLOR_BGR2GRAY)

    if norm == False:
        return pth.reshape(1, dim, dim, channel)

    return cv2.normalize(
        pth,
        None,
        0,
        1,
        cv2.NORM_MINMAX,
        cv2.CV_32F
    ).reshape(1, dim, dim, channel)


with open('model_graynorm_95_batch8_150.json', 'r') as f:
    loaded_model_json = f.read()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("model_graynorm_95_batch8_150x.h5")
print("Loaded model from disk")


shape_to_label = {
    'rock': np.array([1., 0., 0.]),
    'paper': np.array([0., 1., 0.]),
    'scissor': np.array([0., 0., 1.])
}
arr_to_shape = {np.argmax(shape_to_label[x]): x for x in shape_to_label.keys()}

cap = cv2.VideoCapture(0)

# image = cv2.imread('paper/paper0.jpg')
# prediction = loaded_model.predict(prepImg(image))
# print(f'pred = {prediction}')
# print(f'pred = {arr_to_shape[np.argmax(prediction)]}')
# cv2.imshow('im', image)
# cv2.waitKey(0)

# tcltk
# accuracy
# f1 score
# sklearn
# cvscore
# traintest split

while True:
    ret, frame = cap.read()

    frame = cv2.flip(frame, 1)

    image = prepImg(frame[50:350, 100:400])

    prediction = loaded_model.predict(image)
    prediction = [int(x*1000)/10 for x in prediction[0]]
    # prediction = [0, 0, 0, 0]
    pred = arr_to_shape[np.argmax(prediction)]

    print(f'prediction = {prediction}')

    cv2.rectangle(frame, (100, 50), (400, 350), (255, 255, 255), 2)
    frame = cv2.putText(frame, pred, (150, 140),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (250, 250, 0), 2, cv2.LINE_AA)

    cv2.imshow('Rock Paper Scissor', frame)

    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
